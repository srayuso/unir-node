# Repo para EIEC - DevOps - UNIR

Este repositorio incluye un ejemplo sencillo de integración entre Bitbucket y CircleCI. El objetivo es que el alumno entienda la integración y la configuración del proyecto en CircleCI, por lo que el código y la estructura del proyecto son especialmente sencillos. La librería se publica en https://www.npmjs.com/package/unir-node.

El repositorio https://bitbucket.org/srayuso/unir-node-test contiene un pequeño ejemplo de instalación de la librería como dependencia.