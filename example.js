const calculator = require('./'); // require the `index.js` file from the same directory.

console.log(calculator.sum(1, 2));
console.log(calculator.sum(1, -2));
console.log(calculator.substract(2, 2));
console.log(calculator.substract(2, -2));