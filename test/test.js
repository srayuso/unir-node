var assert = require('assert');
const calculator = require('../');

describe('Calculator', function () {
    it('should return a correct sum', function () {
        assert.equal(calculator.sum(1, 2), 3);
    });

    it('should return a correct substraction', function () {
        assert.equal(calculator.substract(1, -2), 3);
    });

    it('should return a correct multiplication', function () {
        assert.equal(calculator.multiply(2, 3), 6);
        assert.equal(calculator.multiply(2, -3), -6);
    });

    it('should return a correct division', function () {
        assert.equal(calculator.divide(6, 3), 2);
        assert.equal(calculator.divide(2, 0), Infinity);
    });

});
